class Pokemon(var name: String, var type: String, var healthPoints: Int, var attackPoints: Int) {

    fun tackle(): Int {
//        println("${name} used tackle")
        return attackPoints
    }

    fun hasFainted(): Boolean {
            return healthPoints<=0
    }
}