import com.sun.org.apache.xpath.internal.operations.Bool
import jdk.nashorn.internal.runtime.regexp.joni.EncodingHelper
import jdk.nashorn.internal.runtime.regexp.joni.EncodingHelper.isDigit
import java.lang.Exception
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*


//fun main() {

//    println("Hello world")
//
//    //mutable
//    var myName="Amiel"
//    println(myName)
//
//    //immutable
//    val mySurname="Morilla"
//    println(mySurname)
//
//    val myAge=21
//    println(myAge)


    //split
//    val myMessage="Happy Birthday! Justin!"
//
//    println(myMessage)
//
//    println(myMessage.split(" ")[2])


    //compareTo
//    val myMessage="Happy Birthday! Justin!"
//    val myOtherMessage="Happy Birthday! Justin!"
//
//    println(myMessage.compareTo(myOtherMessage))

    //equals
//    val myMessage="Happy Birthday! Justin!"
//    val myOtherMessage="Happy Birthday! Justin!"
//
//    println(myMessage.equals(myOtherMessage))

    //equals with ignore case
//    val myMessage="Happy Birthday! Justin!"
//    val myOtherMessage="Happy Birthday! JustiN!"
//
//    println(myMessage.equals(myOtherMessage,false))

    //length
// val myMessage="Happy Birthday! Justin!"
// println(myMessage.length)


    //concat
//    val givenName="John Amiel"
//    val surname="Morilla"
//    val middleName="Rutaquio"
//
//    println("My full name is ${givenName.plus(" "+middleName.plus(" "+surname))}")


    //codewars activity
//    var str="amiel"
//    println(str.subSequence(1,str.length-1).toString());


//    print("Input your name: ")
//    val stringInput = readLine()!!
//    println("Your name is $stringInput")


    //Age calculator
//    val reader = Scanner(System.`in`)
//    print("What is your age? ")
//    var age: Int = reader.nextInt()
//    println("You entered: $age")
//
//    println("When is your birthdate? (yyyy-MM-dd) format please")
//    var bday = readLine()!!
//    println("Your birthday is $bday")
//    var bdayDate=LocalDate.parse(bday)
//
//
//    var date=LocalDate.now().format(DateTimeFormatter.ofPattern("M/d/y"))
//    var newDate= LocalDate.parse(date, DateTimeFormatter.ofPattern("M/d/y"))
//    println("You are ${bdayDate.until(newDate).years} years old.")


//    fun isDigit(str: String): Boolean {
//
//        var sstr = str.trim()
//        try {
//            var num = Integer.parseInt(sstr)
//
////            println(num::class.simpleName)
////            return true;
//        } catch (e: Exception) {
//
//            println(e.localizedMessage)
//            return false;
//        }
//
//
//    }
//    println(isDigit("1.23"))

//}

fun main(args:Array<String>){

    val pokemon1=Pokemon("Bulbasaur","Grass",100,10)
    println("New pokemon encountered: ${pokemon1.name}")
    println("Pokemon type: ${pokemon1.type}")
    println("Pokemon Health points: ${pokemon1.healthPoints}")
    println("Pokemon Attack points: ${pokemon1.attackPoints}")

    println()

    val pokemon2=Pokemon("Charmander","Fire",200,20)
    println("New pokemon encountered: ${pokemon2.name}")
    println("Pokemon type: ${pokemon2.type}")
    println("Pokemon Health points: ${pokemon2.healthPoints}")
    println("Pokemon Attack points: ${pokemon2.attackPoints}")
    println()

    pokemon1.tackle()
    pokemon2.tackle()

    startBattle(pokemon1,pokemon2)
}
fun startBattle(challenger: Pokemon, champion: Pokemon) {


    while (!champion.hasFainted() || !challenger.hasFainted()) {


        var chalDamage = challenger.tackle()

        champion.healthPoints -= chalDamage
        println("${challenger.name}'s attack dealt ${challenger.attackPoints} damage")
        println("${champion.name}'s health is ${champion.healthPoints}")
        if(champion.hasFainted()){
            println("${champion.name} fainted!")
            println("${challenger.name}'s Victory!")
            break;
        }
        var champDamage = champion.tackle()
        challenger.healthPoints -= champDamage
        println("${champion.name}'s attack dealt ${champion.attackPoints} damage")
        println("${challenger.name}'s health is ${challenger.healthPoints}")

        if(challenger.hasFainted()){
            println("${challenger.name} fainted!")
            println("${champion.name}'s Victory!")
            break;
        }
    }
}