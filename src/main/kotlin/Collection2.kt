import java.util.Scanner

fun main() {


//    val numbers = listOf(1,2,3,4,5)
//    //for each -> loops in every item of a list
//    numbers.forEach {numbers->
//
//
//        println(numbers)
//    }
//
//    //map -> will return a new collection, performing the transformation
//    val newNumber=numbers.map {
//        it*4
//        it*4
//        it*5
//    }
//    println(newNumber)
//
//
//    //filter -> will return a new collection, filtering the values based on our condition
//
//
//    val evenNumbers=numbers.filter {
//        it%2==0
//    }
//    println(evenNumbers)
//


//      val numbers = listOf(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)
//
//    for(item in numbers){
//        if(item %3==0&&item%5==0){
//            println("PingPong")
//        }else if(item %3==0){
//            println("Ping")
//        }else if(item %5==0){
//            println("Pong")
//        }else{
//            println("x")
//        }
//    }


//val grades = listOf(
//    listOf(94,82,85),
//    listOf(83,99,97),
//    listOf(76,89,90))
//
//
//   val newGrades= grades.map{
//      println(it.maxOrNull())
//   }


    //display all perfect numbers
    val numbers = (1..1000).toList()

//    println(numbers)



    val perfectNums = numbers.filter { number ->
        var temp = 0
        for (i in 1..number/2) {
            if (number % i == 0) {
                temp += i
            }
        }
     temp==number
    }

    print(perfectNums)
}