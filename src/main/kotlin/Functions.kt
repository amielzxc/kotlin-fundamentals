fun main() {
//    println(isEven(2));
//    println(isEven(3));
//    println(isEven(4));

    val account = mutableMapOf<String, Any>(
        "username" to "Brandon",
        "balance" to 0
    )

    println(checkBalance(account))
    deposit(account, 2000)
    println(checkBalance(account))
    deposit(account, 2000)
    println(checkBalance(account))
    withdraw(account,1500)
    println(checkBalance(account))
}

fun checkBalance(account: Map<String, Any>): String {
    return "Your balance is ${account.getValue("balance")}"
}
fun deposit(account: MutableMap<String, Any>, amount: Int) {

    if(amount>5000){
        println("You cannot deposit more than 5000")
    }else if(amount%100!=0){
        println("You cannot deposit an amount not divisible by 100")
    }else if(amount < 0){
        println("You cannot deposit a negative amount")
    }else {
        val newBal = Integer.parseInt(account.getValue("balance").toString()) + amount
        account.put("balance", newBal);
        // println("Your balance is ${account.getValue("balance")}")
    }
}
fun withdraw(account: MutableMap<String, Any>, amount: Int){
    val halfBal=account.getValue("balance").toString().toInt()/2
    if(amount > halfBal){
        println("You cannot withdraw more than half of your balance")
    }else if(amount < 0){
        println("You cannot withdraw a negative amount")
    }else{
        println("You withdrew an amount of ${amount}")
        val newBal = Integer.parseInt(account.getValue("balance").toString()) - amount
        account.put("balance", newBal);
    }
}


//to check a number is odd or even
//fun isEven(num: Int):Boolean{
// return num % 2 == 0
//
//}